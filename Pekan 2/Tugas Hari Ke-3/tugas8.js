//soal1//////////////////////////////////////
const phi=3.14
const luaslingkaran = (r) => {return phi*r*r}
const kellingkaran = (r) => {return 2*phi*r}
let r=5
console.log(luaslingkaran(r))
console.log(kellingkaran(r))

//soal2//////////////////////////////////////
let kalimat = ""
const kalimatf=()=>{
let k1="saya"
let k2="adalah"
let k3="seorang"
let k4="frontend"
let k5="developer"
kalimat=  `${k1} ${k2} ${k3} ${k4} ${k5}` 
 }
kalimatf()
console.log(kalimat) 
//soal3////////////////////////
class Book {
    constructor(nama,totalpage,price) {
        this._nama = nama;
        this._totalpage=totalpage;
        this._price=price;
      }
      get name() {
        return this._nama;
      } 
      get totalpage() {
        return this._totalpage;
      } 
      get price() {
        return this._price;
      }
}

class Komik extends Book {
    constructor(nama,total,price,color) {
      super(nama,total,price)
      this._isColorful = color;
    }
    get isColorful() {
        return this._isColorful;
      }
  }
var komik1 = new Komik("kera sakti",211,1900,true)
console.log(komik1.name)
console.log(komik1.totalpage)
console.log(komik1.price)
console.log(komik1.isColorful)


  
 