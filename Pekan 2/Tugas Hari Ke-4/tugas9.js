//1 object literal
const newFunction =(firstName, lastName)=>{
    return {
       firstName,
       lastName,
       fullName:()=>
       console.log(firstName+" "+lastName)
    }
  }

//Driver Code 
newFunction("William", "Imoh").fullName() 


//2 destructuring

const newObject = {
 firstName: "Harry",
 lastName: "Potter Holt",
 destination: "Hogwarts React Conf",
 occupation: "Deve-wizard Avocado",
 spell: "Vimulus Renderus!!!"
}
const { firstName, lastName, destination,occupation,spell} = newObject;
console.log(firstName, lastName, destination, occupation,spell)


//3 array spreading
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
let combined= [...west,...east]
//Driver Code
console.log(combined)

